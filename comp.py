#!/usr/bin/env python

import argparse
import glob
import pandas as pd
import re

def parse_csv(file):
    df = pd.read_csv(file, sep="[,|]", header=None, engine='python')
    df.name = file
    return df

def parse_dir(dir):
    frames = []
    files = glob.glob(dir + "*.csv")
    files.sort(key=lambda x:[int(c) if c.isdigit() else c for c in re.split(r'(\d+)', x)])
    for file in files:
        frames.append(parse_csv(file))
    return frames

def df_diff(df1, df2):
    diff = df1 - df2

    confidence = diff[1].std()

    obj_class = diff[2].mean()

    class_prob = 0
    for i in range (3,88):
        class_prob += diff[i].std()
    class_prob /= 84

    image_pos = 0
    for i in range (88,92):
        image_pos += diff[i].std()
    image_pos /= 4

    camera_pos = 0
    for i in range (92,95):
        camera_pos += diff[i].std()
    camera_pos /= 3

    heading_camera = diff[95].std()

    dimensions = 0
    for i in range (96,99):
        dimensions += diff[i].std()
    dimensions /= 3

    vehicle_pos = 0
    for i in range (112,115):
        vehicle_pos += diff[i].std()
    dimensions /= 3

    heading_vehicle = diff[115].std()

    velocity = 0
    for i in range (112,115):
        velocity += diff[i].std()
    velocity /= 3
        
    summary = pd.DataFrame({ 
        'confidence':[confidence],
        'class':[obj_class],
        'class probability':[class_prob],
        'image position':[image_pos],
        'camera position':[camera_pos],
        'heading_camera':[heading_camera],
        'dimensions':[dimensions],
        'vehicle_pos':[vehicle_pos],
        'heading_vehicle':[heading_vehicle],
        'velocity':[velocity]
        })
    return summary

def main():
    parser = argparse.ArgumentParser(description='Enter directories')
    parser.add_argument("dir1")
    parser.add_argument("dir2")
    args = parser.parse_args()
    dir1 = args.dir1
    dir2 = args.dir2

    frames1 = parse_dir(dir1)
    df1 = pd.concat(frames1)

    frames2 = parse_dir(dir2)    
    df2 = pd.concat(frames2)

    frame1_detections = 0
    frame2_detections = 0
    diff_detections = 0
    for i in range(len(frames1)):
        frame1_size = len(frames1[i])
        frame2_size = len(frames2[i])
        print("Frame " + str(i) + " " + str(frame1_size) + " | " + str(frame2_size))
        frame1_detections += frame1_size
        frame2_detections += frame2_size
        if(frame1_size != frame2_size):
            print("Warning: frames have different detection counts (" + str(frame1_size) + " vs " + str(frame2_size) + ")")
            diff_detections += abs(frame1_size-frame2_size)
        print(df_diff(frames1[i], frames2[i]))
    print("Mismatch rows " + str(diff_detections))

    print(df_diff(df1, df2).to_string(index=False))

if __name__=="__main__":
    main()
    